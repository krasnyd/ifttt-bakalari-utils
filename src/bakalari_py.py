import json

import requests
import hashlib
import xml.etree.ElementTree as et
import base64
import time
import datetime


# WIP BakalariApi class - NOT READY FOR USE
class BakalariApi:
    url: str
    token: str

    def __init__(self, bakalariUrl: str):
        self.url = bakalariUrl

    def login(self, username: str, password: str):
        r = requests.get(f"{self.url}/if/2/gethx/{username}")
        gethx = json.loads(r.text)

    def _send_request(self):
        pass



# Code from https://github.com/kokolem/bakalari-next-day/blob/master/bakalariNextDay.py
class Bakalari:  # Trida pro pristup k API bakalaru
    def __init__(self, login, password,
                 adresa):  # Argumenty jsou string s prihlasovacim jmenem, string s heslem a string s url adresou bakalaru skoly. Ze zadanych udaju se vypocita token a ulozi jako self.token

        # Adresa je potreba i v ostatnich metodach, heslo a prihlasovaci jmeno ne (staci token).
        self.adresa = adresa

        # Ziskani zakladnich komponent pro token
        loginInitR = requests.get(adresa, params={"gethx": login})
        loginInitX = et.fromstring(loginInitR.text)
        typ = loginInitX.find("typ").text
        ikod = loginInitX.find("ikod").text
        salt = loginInitX.find("salt").text

        # Vypocet hesla
        passwordHashOriginal = salt + ikod + typ + password
        passwordHashOriginal = passwordHashOriginal.encode("UTF-8")

        # Zahashovani hesla
        passwordHash = hashlib.sha512()
        passwordHash.update(passwordHashOriginal)
        passwordHash = base64.encodebytes(passwordHash.digest()).decode("UTF-8").replace("\n", "")

        # Vypocet tokenu
        tokenOriginal = "*login*" + login + "*pwd*" + passwordHash + "*sgn*ANDR" + time.strftime('%Y%m%d')

        # Zahashovani tokenu
        token = hashlib.sha512()
        orig = tokenOriginal.encode("UTF-8")
        token.update(orig)
        dig = token.digest()
        token = base64.encodebytes(dig)
        token = token.decode("UTF-8")
        token = token.replace("\n", "").replace("/", "_").replace("\\", "_").replace("+", "-")

        self.token = token
        pass

    def rozvrh(self):
        # Ziskani rozvrhu
        scheduleStr = requests.get(self.adresa, params={"hx": self.token, "pm": "rozvrh"})
        schedule = et.fromstring(scheduleStr.text)
        return schedule
