from datetime import datetime
import requests
from time import sleep, time
from dateutil import parser
import traceback

from bakalari_py import Bakalari
from config import bakalariLogin, bakalariPassword, bakalariUrl, iftttToken, nextLessonNotificationEvent


def main():
    print("[STARTING] IFTTT bakalari notifier... [OK]")
    sendNotification("STARTED", "WATCHER")

    # Check the timetable every minute
    while True:
        try:
            b = Bakalari(bakalariLogin, bakalariPassword, bakalariUrl)
            rozvrh = b.rozvrh()
            tyden = SkolniTyden(rozvrh)
            if tyden.today():
                for hodina in tyden.today():
                    start = parser.parse(hodina.startTime)
                    now = datetime.now()
                    delta = start - now
                    if start > now and delta.seconds < 600:
                        sendNotificationIfEnoughTimeElapsed(hodina)

            print(f"[INFO] ({datetime.now()}) Sleeping")
            sleep(120)
        except KeyboardInterrupt:
            print("[STOPPED] IFTTT bakalari notifier")
            return
        except Exception as e:
            print(f"Exception occurred! error: {e}")
            traceback.print_exc()
            sleep(60)


class SkolniTyden():  # Trida pro zpracovavani XML z bakalaru
    def __init__(self, scheduleXML):  # Argument je XML s rozvrhem z Bakalaru
        self.rozvrh = []
        cisloDnu = 0

        # Pro kazdy den v rozvrhu se do self.rozvrh prida prazdne pole
        dny = scheduleXML.find("rozvrh").find("dny").findall("den")
        for den in dny:
            hodiny = den.find("hodiny").findall("hod")
            parsedTimetableTimes = {}
            for time in scheduleXML.find("rozvrh").find("hodiny").findall("hod"):
                caption = time.find("caption").text
                begintime = time.find("begintime").text
                endtime = time.find("endtime").text
                parsedTimetableTimes[caption] = {"caption": caption, "begintime": begintime, "endtime": endtime}

            # Pro kazde pole (reprezentuje den ve skolnim rozvrhu) v self.rozvrh se do nej pridaji nazvy hodin, ktere v ten den jsou
            # for hodina in hodiny:
            #     if hodina.find("pr") != None:
            #         self.rozvrh[cisloDnu].append(hodina.find("pr").text)
            self.rozvrh.append(self._parseDay(hodiny, parsedTimetableTimes))

            cisloDnu += 1

    def _parseDay(self, day: list, parsedTimetableTimes: dict) -> list:
        hodiny = []
        for hodina in day:
            if hodina.find("pr") != None:
                hodiny.append(self.Hodina(hodina, parsedTimetableTimes))
        return hodiny


    class Hodina:
        nazev: str
        ucebna: str
        ucitel: str
        startTime: int
        endTime: int

        def __init__(self, hodinaXML, parsedTimetableTimes: dict):
            self.nazev = hodinaXML.find("pr").text
            self.ucebna = hodinaXML.find("zkrmist").text
            self.ucitel = hodinaXML.find("uc").text

            placeInTimetable = hodinaXML.find("caption").text

            self.startTime = parsedTimetableTimes[placeInTimetable]["begintime"]
            self.endTime = parsedTimetableTimes[placeInTimetable]["endtime"]


    def today(self):
        dayNumber = datetime.today().weekday()
        if dayNumber in [0, 1, 2, 3, 4]:
            return self.rozvrh[dayNumber]


def sendNotification(value1="", value2="", value3=""):
    r = requests.post(
        f"https://maker.ifttt.com/trigger/{nextLessonNotificationEvent}/with/key/{iftttToken}",
        json={"value1": value1, "value2": value2, "value3": value3})
    if r.status_code == 200:
        print(f'[OK] Notification sent for value1="{value1}", value2="{value2}", value3="{value3}".')
    else:
        print(f'[FAILED] Notification sent for value1="{value1}", value2="{value2}", value3="{value3}"!')
        print(f"Error. status_code: {r.status_code}, text: {r.text}")


lastNotificationSent = 0
def sendNotificationIfEnoughTimeElapsed(hodina: SkolniTyden.Hodina):
    global lastNotificationSent
    if lastNotificationSent + 1800 < time():
        sendNotification(hodina.nazev, hodina.ucebna)
        lastNotificationSent = time()



if __name__ == "__main__":
    main()
